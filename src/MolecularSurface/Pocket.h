/*
 * Pocket.h
 *
 *  Created on: Sep 24, 2015
 *      Author: sebastian
 */

#ifndef POCKET_H_
#define POCKET_H_

#include "../Geometry/border_voxel.h"
#include "../Geometry/voxelGrid.h"
#include <vector>

using namespace std;



/**
 * Definition of the Pocket class
 */
class Pocket {
public:
	Pocket();
	Pocket(Pocket const & p);
	Pocket(voxelGrid const & pocketSurface, voxelGrid const & cpkModel);
	Pocket & operator=(Pocket const & p);

	bool isPocket;
//	bool SA_left;  /**> True if the current potential pocket is solvent accessible on the left */
//	bool SA_right; /**> True if the current potential pocket is solvent accessible on the right */
	vector<bool> SA_left;
	vector<bool> SA_right;
	vector<voxel> pocketVoxels; /**> List containing all the voxels
									* composing the surface of the pocket. */
	vector<vector<border_voxel>> leftBorder; /**> Voxels belonging to the current voxels
	 	 	 	 	 	 	 	  * on the left extremity of the current slice.
	 	 	 	 	 	 	 	  * Empty if no such voxels. */
	vector<vector<border_voxel>> rightBorder;/**> Voxels belonging to the current voxels
 	 	  	  	  	  	  	  	  * on the right extremity of the current slice.
 	 	  	  	  	  	  	  	  * Empty if no such voxels. */
	bool hasLeftBorder() const;
	bool hasRightBorder() const;
};



#endif /* POCKET_H_ */
