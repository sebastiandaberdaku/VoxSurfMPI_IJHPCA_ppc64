/*
 * MolecularSurface.h
 *
 *  Created on: Nov 11, 2013
 *      Author: daberdaku
 */
/**
 * This header defines the MolecularSurface object, representing
 * the surface of a molecule, with the related methods for its
 * calculation.
 */
#ifndef MOLECULARSURFACE_H_
#define MOLECULARSURFACE_H_

#include "../Geometry/point3D.h"
#include "../Geometry/voxelGrid.h"
#include "../PDB/atom.h"
#include "Pocket.h"
#include <list>
#include <queue>
#include <vector>

using namespace std;

class MolecularSurface {
	friend class Molecule;
public:
	voxelGrid *cpkModel, *surface;
	float resolution;
	vector<Pocket> pockets;

	MolecularSurface(vector<atom> const & atoms, float probeRadius,
			float resolution, uint16_t length, uint16_t width, uint16_t height,
			point3D translation);
	virtual ~MolecularSurface();

	static void boundingBox(vector<atom> const & atomsInModel, bool addProbeRadius,
			float probeRadius, float resolution, uint16_t & length,
			uint16_t & width, uint16_t & height, point3D & translation,
			float & max_atm_radius, float & min_atm_radius);
	void createCPKModel(bool addProbeRadius);

	void removeSolventExcludedVoids();

	void buildSurface(bool addProbeRadius);

	void fastRegionGrowingEDT();
	void extractPotentialPockets();

	void outputSurfacePCDModel(string const & filename);
	void outputSurfacePCDModel(string const & filename, double const R[3][3], point3D const & T);

	void outputPocketsPCDModel(string const & filename);
	void outputPocketsPCDModel(string const & filename, double const R[3][3], point3D const & T);

	void outputSurfaceOpenDXModel(string const & filename);

	void solveUndeterminedPockets();
private:
	point3D ptran;
	vector<atom> const * atoms;
	float probeRadius;
	uint16_t pheight, pwidth, plength;
};
#endif /* MOLECULARSURFACE_H_ */
