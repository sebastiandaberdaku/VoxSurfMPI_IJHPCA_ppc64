/*
 * MolecularSurface.cpp
 *
 *  Created on: Dec 23, 2013
 *      Author: daberdaku
 */
/**
 * Implementation of the MolecularSurface class. This class
 * defines the molecular surface object, with all the methods
 * needed for its calculation.
 */
#include "../exceptions/ParsingPDBException.h"
#include "../Geometry/dmap_value.h"
#include "../Geometry/HierarchicalQueue.h"
#include "../Geometry/partialEDMap.h"
#include "../Geometry/SeedFill3D/rapid3DSeedFill.h"
#include "../Geometry/SeedFill3D/rapid3DSurfaceExtract.h"
#include "../Geometry/SeedFill3D/rapid3DPocketExtract.h"
#include "../Geometry/SeedFill3D/rapid3DOuterSurfaceExtract.h"
#include "../Geometry/Sphere/DrawSphere.h"

#include "../utils/disclaimer.h"

#include "../utils/makeDirectory.h"
#include "../utils/numerical_limits.h"
#include "MolecularSurface.h"
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>

#include <sstream>
#include <stdio.h>
#include <stdlib.h>

#include <mpi.h>

/**
 * Constructor of the class. Initializes the voxel grid and other data structures.
 *
 * \param atoms				List containing the atoms of the molecule.
 * \param probeRadius		Desired probe radius of the solvent rolling sphere.
 * \param resolution		resolution^3 = number of voxels per Å^3
 * \param isPQR				true if the atom list comes from a PQR file, false
 * 							if it comes from a PDB file
 * \param addProbeRadius	true: add the probe radius to the atomic radius
 * 							values, SAS and MS (SES) have this value set to true;
 * 							false: use original atomic radius values.
 * \param length			Length of the voxel grid.
 * \param width				Width of the voxel grid.
 * \param height			Height of the voxel grid.
 * \param translation		Translation vector to the grid's coordinate system
 * 							(already scaled).
 *
 */
MolecularSurface::MolecularSurface(std::vector<atom> const & atoms,
		float probeRadius, float resolution,
		uint16_t length, uint16_t width, uint16_t height, point3D translation) :
		atoms(&atoms), probeRadius(probeRadius), resolution(resolution),
		plength(length), pwidth(width), pheight(height), ptran(translation),
		cpkModel(NULL), surface(NULL) {
	if (atoms.empty())
		throw invalid_argument("MolecularSurface::MolecularSurface() - The molecule has no atoms!");

} /* MolecularSurface() */

/**
 * Destructor of the class.
 */
MolecularSurface::~MolecularSurface() {
	if (cpkModel != NULL)
		delete cpkModel;
	if (surface != NULL)
		delete surface;
} /* ~MolecularSurface() */

/** Calculates a bounding box containing the molecule. This method
 * calculates the maximal and minimal coordinates reached by the
 * molecule by checking all the coordinates of the atoms composing it.
 *
 * \param atomsInModel		List containing all the model's atoms.
 * \param probeRadius		Desired probe radius of the solvent rolling sphere.
 * \param resolution		resolution^3 = number of voxels per Å^3
 * \param length			(return value) the length of the voxel grid.
 * \param width				(return value) the width of the voxel grid.
 * \param height			(return value) the height of the voxel grid.
 * \param translation		(return value) the translation vector to the
 * 							grid's coordinate system (already scaled).
 * \param max_atm_radii		(return value) the maximum atomic radii in
 * 							atomsInModel
 */
void MolecularSurface::boundingBox(std::vector<atom> const & atomsInModel,
		bool addProbeRadius, float probeRadius, float resolution, uint16_t & length,
		uint16_t & width, uint16_t & height, point3D & translation,
		float & max_atm_radius, float & min_atm_radius) {
	if (atomsInModel.empty())
		throw ParsingPDBException("No atoms in PDB model.",
				"MolecularSurface::boundingBox", "Probably due to corrupt PDB file.");
	point3D minp(max_float, max_float, max_float);
	point3D maxp(min_float, min_float, min_float);
	for (auto const & atm : atomsInModel) {
		if (atm.x < minp.x)
			minp.x = atm.x;
		if (atm.y < minp.y)
			minp.y = atm.y;
		if (atm.z < minp.z)
			minp.z = atm.z;
		if (atm.x > maxp.x)
			maxp.x = atm.x;
		if (atm.y > maxp.y)
			maxp.y = atm.y;
		if (atm.z > maxp.z)
			maxp.z = atm.z;
		if (max_atm_radius < atm.radius)
			max_atm_radius = atm.radius;
		if (min_atm_radius > atm.radius)
			min_atm_radius = atm.radius;
	}
	float d;
	if (addProbeRadius) // SAS or SES
		d = max_atm_radius + probeRadius + 0.5;
	else // vdW
		d = max_atm_radius + 0.5;

	maxp += point3D(d, d, d);
	minp -= point3D(d, d, d);

	/* transformation values */
	translation = -minp;

	/* bounding box dimensions */
	double boxLength = ceil(resolution * (maxp.x - minp.x));
	double boxWidth  = ceil(resolution * (maxp.y - minp.y));
	double boxHeight = ceil(resolution * (maxp.z - minp.z));

	if (boxLength <= UINT16_MAX && boxWidth <= UINT16_MAX && boxHeight <= UINT16_MAX) {
		length = static_cast<uint16_t>(boxLength);
		width  = static_cast<uint16_t>(boxWidth);
		height = static_cast<uint16_t>(boxHeight);
	} else {
		std::stringstream ss;
		ss << "MolecularSurface::boundingBox() - ";
		ss << "The bounding box's dimensions exceed the maximum value of " << UINT16_MAX << ". ";
		ss << "Try setting a lower \"resolution\" value.";
		throw std::invalid_argument(ss.str());
	}
} /* boundingBox() */

/** Fills the voxels in the grid occupied by the molecule (protein).
 * This method implements a space-filling algorithm which is the
 * preliminary step for our grid-based macro-molecular surface
 * generation.
 *
 * In chemistry, a space-filling model, also known as a calotte model,
 * is a type of three-dimensional molecular model where the atoms are
 * represented by spheres whose radii are proportional to the radii of
 * the atoms and whose center-to-center distances are proportional to
 * the distances between the atomic nuclei, all in the same scale.
 * Atoms of different chemical elements are usually represented by
 * spheres of different colors.
 *
 * Calotte models are distinguished from other 3D representations,
 * such as the ball-and-stick and skeletal models, by the use of "full
 * size" balls for the atoms. They are useful for visualizing the
 * effective shape and relative dimensions of the molecule, in particular
 * the region of space occupied by it. On the other hand, calotte models
 * do not show explicitly the chemical bonds between the atoms, nor the
 * structure of the molecule beyond the first layer of atoms.
 *
 * Space-filling models are also called CPK models after the chemists
 * Robert Corey, Linus Pauling and Walter Koltun, who pioneered their use.
 */
void MolecularSurface::createCPKModel(bool addProbeRadius) {
	if (cpkModel != NULL)
		delete cpkModel;
	cpkModel = new voxelGrid(plength, pwidth, pheight);
	int cx, cy, cz; /**< Discretized coordinates of the atom's center. */
	int radius; /**< radius */

	/* For every atom in our list, calculate the voxels it occupies. */
	for (auto const & atm : *atoms) {
		/* Translate and discretize the coordinates */
		cx = static_cast<int>(round((atm.x + ptran.x) * resolution));
		cy = static_cast<int>(round((atm.y + ptran.y) * resolution));
		cz = static_cast<int>(round((atm.z + ptran.z) * resolution));
		float r = atm.radius * resolution;
		if (addProbeRadius)
			r += probeRadius * resolution;
		radius = static_cast<int>(r + 0.5);
		if (cx + radius < 0 || cx - radius >= plength)
			continue;
		DrawBall(*cpkModel, cx, cy, cz, radius);
	}
} /* createCPKModel() */

void MolecularSurface::removeSolventExcludedVoids() {
	/* Determine the starting point for the flood fill algorithm.
	 * Start by testing an edge voxel of the bounding box.
	 * If an adequate resolution is used, the edges should not be
	 * occupied by any atom. This possibility indicates an erroneous
	 * calculation of the protein surface. */
	assert(!cpkModel->getVoxel(0, 0, 0));

	if (surface != NULL)
		delete surface;
	surface = new voxelGrid(plength, pwidth, pheight);
	/* Call to the three-dimensional flood fill algorithm to
	 * "color" the all voxels lying between the bounding box's
	 * boundaries and the outer surface of the volumetric model.
	 */
//	int leap = static_cast<int>(resolution * probeRadius);
	rapid3DOuterSurfaceExtract::outerSurfaceExtract(*cpkModel, *surface, voxel(0, 0, 0));

} /* fillInternalCavities() */

/**
 * Build the boundary of the solid created with the space-filling algorithm.
 * A voxel belongs to the boundary if it has at least one neighbor which is not
 * occupied by any atom.
 */
void MolecularSurface::buildSurface(bool addProbeRadius) {
	if (surface != NULL)
		delete surface;
//	surface = new voxelGrid(*cpkModel);
	surface = new voxelGrid(plength, pwidth, pheight); /**< already initialized to all zeros */
	int cx, cy, cz;
	int radius;

	/*
	 * find a first voxel belonging to the CPKmodel
	 * and use it as a seed;
	 */
	voxelGrid temp(*cpkModel);
	/*
	 * the model could have disjoint parts,
	 * all atom centers must be used as seeds
	 */
	for (auto const & atm : *atoms) {
		/* Translate and discretize the coordinates */
		cx = static_cast<int>(round((atm.x + ptran.x) * resolution));
		cy = static_cast<int>(round((atm.y + ptran.y) * resolution));
		cz = static_cast<int>(round((atm.z + ptran.z) * resolution));
		if (addProbeRadius)
			radius = static_cast<int>(round((atm.radius + probeRadius) * resolution));
		else
			radius = static_cast<int>(round(atm.radius * resolution));

		if (cx + radius < 0 || cx - radius >= plength)
			continue;
		else if (cx < 0) {
			if (temp.getVoxel(0, cy, cz))
				rapid3DSurfaceExtract::surfaceExtract3D(*cpkModel, temp, *surface, voxel(0, cy, cz));
			else
				continue;
		}
		else if (cx >= plength) {
			if (temp.getVoxel(plength - 1, cy, cz))
				rapid3DSurfaceExtract::surfaceExtract3D(*cpkModel, temp, *surface, voxel(plength - 1, cy, cz));
			else
				continue;
		}
		else if (temp.getVoxel(cx, cy, cz)) {
			rapid3DSurfaceExtract::surfaceExtract3D(*cpkModel, temp, *surface, voxel(cx, cy, cz));
		}

	}

} /* buildSurface() */

/** Calculates a partial Euclidean Distance Map of the voxelized
 * representation of the molecular surface. Starting from the Solvent-
 * Accessible Surface of the molecule, it is possible to obtain the
 * molecular surface (or Solvent-Excluded Surface) applying the EDT
 * (Euclidean Distance Transform). The SES is then extracted from the
 * partial Euclidean Distance Map, as it's voxels have a particular
 * distance value, i.e. greater or equal to the solvent/probe sphere
 * radius. The calculation starts from the boundary (surface) voxels,
 * and procedes towards the inside of the molecule, one shell at a time.
 * The initial shell is obviously the surface of the molecule. The
 * second shell is composed by voxels internal to the molecule, that
 * are neighbors to the first shell's voxels, and don't belong to the
 * first shell. The third shell will be composed by neighbors of the
 * second shell's voxels, internal to the molecule, and that don't
 * belong to the first or second shell, and so on. The distance map
 * values are propagated from the molecular surface towards the inside
 * of the molecule one shell at a time.
 */
void MolecularSurface::fastRegionGrowingEDT() {
////	voxel * nearestSurfaceVoxel = new voxel[plength * pwidth * pheight];
//	vector<vector<vector<voxel>>> nearestSurfaceVoxel(plength, vector<vector<voxel>>(pwidth, vector<voxel>(pheight)));
//
////	uint16_t * distanceMap = new uint16_t[plength * pwidth * pheight];
//	vector<vector<vector<uint16_t>>> distanceMap(plength, vector<vector<uint16_t>>(pwidth, vector<uint16_t>(pheight, UINT16_MAX)));

	voxel ***nearestSurfaceVoxel = new voxel**[plength];
	for (int i = 0; i < plength; ++i) {
		nearestSurfaceVoxel[i] = new voxel*[pwidth];
	}
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			nearestSurfaceVoxel[i][j] = new voxel[pheight];
		}
	}
	uint16_t ***distanceMap = new uint16_t**[plength];
	for (int i = 0; i < plength; ++i) {
		distanceMap[i] = new uint16_t*[pwidth];
	}
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			distanceMap[i][j] = new uint16_t[pheight];
		}
	}

	uint16_t numberOfQueues = static_cast<uint16_t>(pow(probeRadius * resolution - sqrt(2.0), 2.0));
	HierarchicalQueue* HQ1 = new HierarchicalQueue(numberOfQueues);
	HierarchicalQueue* HQ2 = new HierarchicalQueue(numberOfQueues);

	/* for all voxels */
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0; k < pheight; ++k) {
				/* create the first shell by pushing the surface voxels in the list */
				if (surface->getVoxel(i, j, k)) {
					HQ1->push(voxel(i, j, k), 0);
					/* a surface voxel has zero distance from itself */
					nearestSurfaceVoxel[i][j][k] = voxel(i, j, k);
					distanceMap[i][j][k] = 0;
				}
				else {
					distanceMap[i][j][k] = UINT16_MAX;
				}
			}
		}
	}

	while (!HQ1->empty()) {
		/*current voxel*/
		voxel cv = HQ1->front();
		HQ1->pop();
		voxel nearestSurfVox = nearestSurfaceVoxel[cv.ix][cv.iy][cv.iz];
		uint16_t squaredDistance = distanceMap[cv.ix][cv.iy][cv.iz];
		voxel nb; // neighbour
		bool isEnd = true;
		for (int i = 0; i < 26; ++i) {
			nb = cv + voxel_offset::neighbours[i];
			if (nb.ix >= 0 && nb.ix < plength && cpkModel->getVoxel(nb)) {
				uint16_t newSDistance = nb.sDistance_to(nearestSurfVox);
				if (newSDistance < distanceMap[nb.ix][nb.iy][nb.iz]
						&& newSDistance < numberOfQueues) {
					distanceMap[nb.ix][nb.iy][nb.iz] = newSDistance;
					nearestSurfaceVoxel[nb.ix][nb.iy][nb.iz] = nearestSurfVox;
					HQ1->push(nb, newSDistance);
					isEnd = false;
				}
			}
		}
		if (isEnd && squaredDistance >= 24) {
			HQ2->push(cv, squaredDistance);
		}
	}
	while (!HQ2->empty()) {
		/*current voxel*/
		voxel cv = HQ2->front();
		HQ2->pop();
		voxel nearestSurfVox = nearestSurfaceVoxel[cv.ix][cv.iy][cv.iz];
		// neighbor
		voxel nb;
		for (int i = 0; i < 124; ++i) {
			nb = cv + voxel_offset::neighbours[i];
			if (nb.ix >= 0 && nb.ix < plength && cpkModel->getVoxel(nb)) {
				uint16_t newSDistance = nb.sDistance_to(nearestSurfVox);
				if (newSDistance < distanceMap[nb.ix][nb.iy][nb.iz]
											   && newSDistance < numberOfQueues) {
					distanceMap[nb.ix][nb.iy][nb.iz] = newSDistance;
					nearestSurfaceVoxel[nb.ix][nb.iy][nb.iz] = nearestSurfVox;
					HQ2->push(nb, newSDistance);
				}
			}
		}
	}

	int procs;
	MPI_Comm_size(MPI_COMM_WORLD, &procs);
	if (procs != 1) {
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Status status; /**< MPI_Status structures are used by the message receiving functions to return data about a message.
						 *  The structure contains the following fields:
						 *  - MPI_SOURCE - id of processor sending the message
						 *  - MPI_TAG - the message tag
						 *  - MPI_ERROR - error status */

	MPI_Datatype MPI_DMAP_VALUE; // distance map value contains the current voxel, distance map value and nearest surface voxel
	MPI_Type_contiguous(7, MPI_INT, &MPI_DMAP_VALUE); /* Creates a contiguous datatype */
	MPI_Type_commit(&MPI_DMAP_VALUE); /* Commits the datatype */

	/**
	 * These data structures hold the distance map values of the current slice's border
	 * calculated at the previous iteration.
	 */
	vector<vector<uint16_t>> leftBorder(pwidth, vector<uint16_t>(pheight, UINT16_MAX));
	vector<vector<uint16_t>> rightBorder(pwidth, vector<uint16_t>(pheight, UINT16_MAX));


	int snd = 1;

	while (snd > 0) {
		vector<dmap_value> toLeft, toRight;
		/**
		 * Check if the current border voxels have any betterment compared to the
		 * previous iteration. If the better values come from the opposite direction
		 * of the current border, propatage them to the corresponding adjacent slice.
		 */
		if (rank > 0) {
			for (int j = 0; j < pwidth; ++j) {
				for (int k = 0; k < pheight; ++k) {
					int d = distanceMap[0][j][k];
					voxel const & nb = nearestSurfaceVoxel[0][j][k];
					if (d < leftBorder[j][k] && nb.ix >= 0) {
						toLeft.push_back(dmap_value(0, j, k, d, nb.ix, nb.iy, nb.iz));
						leftBorder[j][k] = d;
					}
				}
			}
		}
		if (rank < procs - 1) {
			for (int j = 0; j < pwidth; ++j) {
				for (int k = 0; k < pheight; ++k) {
					int d = distanceMap[plength - 1][j][k];
					voxel const & nb = nearestSurfaceVoxel[plength - 1][j][k];
					if (d < rightBorder[j][k] && nb.ix < plength) {
						toRight.push_back(dmap_value(plength - 1, j, k, d, nb.ix - plength + 1, nb.iy, nb.iz));
						rightBorder[j][k] = d;
					}
				}
			}
		}
		unsigned long toLeft_N = toLeft.size(), toRight_N = toRight.size(),
				fromLeft_N = 0, fromRight_N = 0;

		vector<dmap_value> fromLeft, fromRight;

//		MPI_Barrier(MPI_COMM_WORLD);
		/**
		 * send to right and receive from left
		 */
		if (rank < procs - 1)
			MPI_Send(&toRight_N, 1, MPI_UNSIGNED_LONG, rank + 1, 0, MPI_COMM_WORLD);
		if (rank > 0)
			MPI_Recv(&fromLeft_N, 1, MPI_UNSIGNED_LONG, rank - 1, 0, MPI_COMM_WORLD, &status);
		/**
		 * send to left and receive from right
		 */
		if (rank > 0)
			MPI_Send(&toLeft_N, 1, MPI_UNSIGNED_LONG, rank - 1, 0, MPI_COMM_WORLD);
		if (rank < procs - 1)
			MPI_Recv(&fromRight_N, 1, MPI_UNSIGNED_LONG, rank + 1, 0, MPI_COMM_WORLD, &status);

//		MPI_Barrier(MPI_COMM_WORLD);
		/**
		 * send to right and receive from left
		 */
		if (rank < procs - 1 && toRight_N > 0)
			MPI_Send(&toRight[0], toRight_N, MPI_DMAP_VALUE, rank + 1, 0, MPI_COMM_WORLD);
		if (rank > 0 && fromLeft_N > 0) {
			fromLeft.resize(fromLeft_N);
			MPI_Recv(&fromLeft[0], fromLeft_N, MPI_DMAP_VALUE, rank - 1, 0, MPI_COMM_WORLD, &status);
		}
		/**
		 * send to left and receive from right
		 */
		if (rank > 0 && toLeft_N > 0)
			MPI_Send(&toLeft[0], toLeft_N, MPI_DMAP_VALUE, rank - 1, 0, MPI_COMM_WORLD);
		if (rank < procs - 1 && fromRight_N > 0) {
			fromRight.resize(fromRight_N);
			MPI_Recv(&fromRight[0], fromRight_N, MPI_DMAP_VALUE, rank + 1, 0, MPI_COMM_WORLD, &status);
		}
		/**
		 * initialize the Hierarchical Queues with the received border voxels
		 */
		for (auto const & v : fromLeft) {
			int d_old = distanceMap[0][v.iy][v.iz];
			if (d_old >= v.d) {
				HQ1->push(voxel(0, v.iy, v.iz), v.d);
				/* a surface voxel has zero distance from itself */
				nearestSurfaceVoxel[0][v.iy][v.iz] = voxel(v.nx, v.ny, v.nz);
				distanceMap[0][v.iy][v.iz] = v.d;
			}
		}
		for (auto const & v : fromRight) {
			int d_old = distanceMap[plength - 1][v.iy][v.iz];
			if (d_old >= v.d) {
				HQ1->push(voxel((plength - 1), v.iy, v.iz), v.d);
				/* a surface voxel has zero distance from itself */
				nearestSurfaceVoxel[plength - 1][v.iy][v.iz] = voxel(v.nx + plength - 1, v.ny, v.nz);
				distanceMap[plength - 1][v.iy][v.iz] = v.d;
			}
		}

		while (!HQ1->empty()) {
			/*current voxel*/
			voxel cv = HQ1->front();
			HQ1->pop();
			voxel nearestSurfVox = nearestSurfaceVoxel[cv.ix][cv.iy][cv.iz];
			uint16_t squaredDistance = distanceMap[cv.ix][cv.iy][cv.iz];
			voxel nb; // neighbour
			bool isEnd = true;
			for (int i = 0; i < 26; ++i) {
				nb = cv + voxel_offset::neighbours[i];
				if (nb.ix >= 0 && nb.ix < plength && cpkModel->getVoxel(nb)) {
					uint16_t newSDistance = nb.sDistance_to(nearestSurfVox);
					if (newSDistance < distanceMap[nb.ix][nb.iy][nb.iz]
						&& newSDistance < numberOfQueues) {
						distanceMap[nb.ix][nb.iy][nb.iz] = newSDistance;
						nearestSurfaceVoxel[nb.ix][nb.iy][nb.iz] = nearestSurfVox;
						HQ1->push(nb, newSDistance);
						isEnd = false;
					}
				}
			}
			if (isEnd && squaredDistance >= 24) {
				HQ2->push(cv, squaredDistance);
			}
		}
		while (!HQ2->empty()) {
			/*current voxel*/
			voxel cv = HQ2->front();
			HQ2->pop();
			voxel nearestSurfVox = nearestSurfaceVoxel[cv.ix][cv.iy][cv.iz];
			// neighbor
			voxel nb;
			for (int i = 0; i < 124; ++i) {
				nb = cv + voxel_offset::neighbours[i];
				if (nb.ix >= 0 && nb.ix < plength && cpkModel->getVoxel(nb)) {
					uint16_t newSDistance = nb.sDistance_to(nearestSurfVox);
					if (newSDistance < distanceMap[nb.ix][nb.iy][nb.iz]
												   && newSDistance < numberOfQueues) {
						distanceMap[nb.ix][nb.iy][nb.iz] = newSDistance;
						nearestSurfaceVoxel[nb.ix][nb.iy][nb.iz] = nearestSurfVox;
						HQ2->push(nb, newSDistance);
					}
				}
			}
		}
		int send_local;
		if (toLeft_N > 0 || toRight_N > 0)
			send_local = 1;
		else
			send_local = 0;
//		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Allreduce(&send_local, &snd, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	} // while
	MPI_Type_free(&MPI_DMAP_VALUE);
	}

	/* update the volumetric model */
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0; k < pheight; ++k) {
				if (distanceMap[i][j][k] < numberOfQueues){
					cpkModel->clearVoxel(i, j, k);
				}
			}
		}
	}

	/* Delete the partial Euclidean Distance Map. */
	for (int i = 0; i < plength; i++) {
		for (int j = 0; j < pwidth; j++) {
			delete[] nearestSurfaceVoxel[i][j];
			delete[] distanceMap[i][j];
		}
	}
	for (int i = 0; i < plength; i++) {
		delete[] nearestSurfaceVoxel[i];
		delete[] distanceMap[i];
	}
	delete[] nearestSurfaceVoxel;
	nearestSurfaceVoxel = NULL;
	delete[] distanceMap;
	distanceMap = NULL;

	delete HQ1;
	HQ1 = NULL;
	delete HQ2;
	HQ2 = NULL;
} /* fastRegionGrowingEDT() */

/** Prints the 3D voxelized representation to file using the PCD
 * (Point Cloud Data) file format.
 *
 * Each PCD file contains a header that identifies and declares
 * certain properties of the point cloud data stored in the file.
 * The header of a PCD must be encoded in ASCII.
 * Storing point cloud data in both a simple ascii form with each
 * point on a line, space or tab separated, without any other
 * characters on it, as well as in a binary dump format, allows
 * us to have the best of both worlds: simplicity and speed,
 * depending on the underlying application. The ascii format
 * allows users to open up point cloud files and plot them using
 * standard software tools like gnuplot or manipulate them using
 * tools like sed, awk, etc.
 *
 * For a detailed description of the PCD (Point Cloud Data) file
 * format specification see:
 * http://pointclouds.org/documentation/tutorials/pcd_file_format.php
 *
 * \param filename	Name of the output file. The '.pcd' extension
 * 					is added automatically.
 *
 * \throws ofstream::failure
 */
void MolecularSurface::outputSurfacePCDModel(std::string const & filename) {
	std::ofstream file_stream;
	makeDirectory("./output");
	file_stream.open("./output/" + filename + ".pcd");
	if (!file_stream.is_open()) {
		throw std::ofstream::failure("Error opening output file.");
	}
	file_stream  << std::setprecision(11);
	std::queue<voxel> surfaceVoxels;
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0; k < pheight; ++k) {
				if (surface->getVoxel(i, j, k))
					surfaceVoxels.push(voxel(i, j, k));
			}
		}
	}
	/* File header */
	file_stream << "# .PCD v.7 - Point Cloud Data file format\n"
			<< "# created with " << PROGRAM_NAME <<"\n"
			<< "# version " << PROGRAM_VERSION <<"\n"
			<< "VERSION .7\n" << "FIELDS x y z\n" << "SIZE 4 4 4\n"
			<< "TYPE F F F\n" << "COUNT 1 1 1\n" << "WIDTH "
			<< surfaceVoxels.size() << "\n" << "HEIGHT 1\n"
			<< "VIEWPOINT 0 0 0 1 0 0 0\n" << "POINTS " << surfaceVoxels.size()
			<< "\n" << "DATA ascii";

	while (!surfaceVoxels.empty()) {
		file_stream << "\n" << surfaceVoxels.front().ix / resolution - ptran.x
				<< " " << surfaceVoxels.front().iy / resolution - ptran.y
				<< " " << surfaceVoxels.front().iz / resolution - ptran.z;
		surfaceVoxels.pop();
	}
	file_stream.close();
} /* outputSurfacePCDModel() */
void MolecularSurface::outputSurfacePCDModel(std::string const & filename, double const R[3][3], point3D const & T) {
	std::ofstream file_stream;
	makeDirectory("./output");
	file_stream.open("./output/" + filename + ".pcd");
	if (!file_stream.is_open()) {
		throw std::ofstream::failure("Error opening output file.");
	}
	file_stream  << std::setprecision(11);
	std::queue<voxel> surfaceVoxels;
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0; k < pheight; ++k) {
				if (surface->getVoxel(i, j, k))
					surfaceVoxels.push(voxel(i, j, k));
			}
		}
	}
	/* File header */
	file_stream << "# .PCD v.7 - Point Cloud Data file format\n"
			<< "# created with " << PROGRAM_NAME <<"\n"
			<< "# version " << PROGRAM_VERSION <<"\n"
			<< "VERSION .7\n" << "FIELDS x y z\n" << "SIZE 4 4 4\n"
			<< "TYPE F F F\n" << "COUNT 1 1 1\n" << "WIDTH "
			<< surfaceVoxels.size() << "\n" << "HEIGHT 1\n"
			<< "VIEWPOINT 0 0 0 1 0 0 0\n" << "POINTS " << surfaceVoxels.size()
			<< "\n" << "DATA ascii";

	while (!surfaceVoxels.empty()) {
		point3D p(surfaceVoxels.front().ix / resolution - ptran.x,
				surfaceVoxels.front().iy / resolution - ptran.y,
				surfaceVoxels.front().iz / resolution - ptran.z);
		p = p.transform(R);
		p += T;
		file_stream << "\n" << p.x << " " << p.y << " " << p.z;
		surfaceVoxels.pop();
	}
	file_stream.close();
}
/**
 * This method extracts potential pocket voxels.
 */
void MolecularSurface::extractPotentialPockets() {
	voxelGrid volumetricModel(*cpkModel);
	rapid3DSeedFill::seedFill3D(volumetricModel, voxel(0, 0, 0)/*, leap*/);
	voxelGrid temp(volumetricModel);
	for (int j = 0; j < pwidth; ++j) {
		for (int k = 0; k < pheight; ++k) {
			/* if the current voxel is inside the molecule */
			if (!volumetricModel.getVoxel(0, j, k)) {
				voxelGrid currentPocket(plength, pwidth, pheight);
				rapid3DPocketExtract::pocketExtract3D(volumetricModel, temp, currentPocket,
						voxel(0, j, k));
				pockets.push_back(Pocket(currentPocket, volumetricModel));
				//fill pocket void in cpkModel
				volumetricModel = temp;
			} // if
			if (!volumetricModel.getVoxel(plength - 1, j, k)) {
				voxelGrid currentPocket(plength, pwidth, pheight);
				rapid3DPocketExtract::pocketExtract3D(volumetricModel, temp, currentPocket,
						 voxel(plength - 1, j, k));
				pockets.push_back(Pocket(currentPocket, volumetricModel));
				//fill pocket void in cpkModel
				volumetricModel = temp;
			} // if
		} // for k
	} // for j
}

void MolecularSurface::outputPocketsPCDModel(std::string const & filename) {
	size_t n_voxels = 0;
	for (auto const & p : pockets) {
		if(p.isPocket)
			n_voxels += p.pocketVoxels.size();
	}
	if(n_voxels == 0)
		return;
	std::ofstream file_stream;
	makeDirectory("./output");
	file_stream.open("./output/" + filename + "-pockets.pcd");
	if (!file_stream.is_open()) {
		throw std::ofstream::failure("Error opening output file.");
	}
	file_stream  << std::setprecision(11);

	/* File header */
	file_stream << "# .PCD v.7 - Point Cloud Data file format\n"
			<< "# created with " << PROGRAM_NAME <<"\n"
			<< "# version " << PROGRAM_VERSION <<"\n"
			<< "VERSION .7\n" << "FIELDS x y z\n" << "SIZE 4 4 4\n"
			<< "TYPE F F F\n" << "COUNT 1 1 1\n" << "WIDTH "
			<< n_voxels << "\n" << "HEIGHT 1\n"
			<< "VIEWPOINT 0 0 0 1 0 0 0\n" << "POINTS " << n_voxels
			<< "\n" << "DATA ascii";
	for (auto const & p : pockets) {
		if (p.isPocket) {
			for (auto const & v : p.pocketVoxels) {
				point3D cp = (point3D(v.ix, v.iy, v.iz) / resolution) - ptran;
				file_stream << "\n" << cp.x << " " << cp.y << " " << cp.z;
			}
		}
	}
	file_stream.close();
}
void MolecularSurface::outputPocketsPCDModel(std::string const & filename, double const R[3][3], point3D const & T) {
	size_t n_voxels = 0;
	for (auto const & p : pockets) {
		if(p.isPocket)
			n_voxels += p.pocketVoxels.size();
	}
	if(n_voxels == 0)
		return;
	std::ofstream file_stream;
	makeDirectory("./output");
	file_stream.open("./output/" + filename + "-pockets.pcd");
	if (!file_stream.is_open()) {
		throw std::ofstream::failure("Error opening output file.");
	}
	file_stream  << std::setprecision(11);
	/* File header */
	file_stream << "# .PCD v.7 - Point Cloud Data file format\n"
			<< "# created with " << PROGRAM_NAME <<"\n"
			<< "# version " << PROGRAM_VERSION <<"\n"
			<< "VERSION .7\n" << "FIELDS x y z\n" << "SIZE 4 4 4\n"
			<< "TYPE F F F\n" << "COUNT 1 1 1\n" << "WIDTH "
			<< n_voxels << "\n" << "HEIGHT 1\n"
			<< "VIEWPOINT 0 0 0 1 0 0 0\n" << "POINTS " << n_voxels
			<< "\n" << "DATA ascii";
	for (auto const & p : pockets) {
		if (p.isPocket) {
			for (auto const & v : p.pocketVoxels) {
				point3D cp = (point3D(v.ix, v.iy, v.iz) / resolution) - ptran;
				cp = cp.transform(R);
				cp += T;
				file_stream << "\n" << cp.x << " " << cp.y << " " << cp.z;
			}
		}
	}
	file_stream.close();
}

void MolecularSurface::solveUndeterminedPockets() {
	int rank;
	int procs;
	MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Status status; /**< MPI_Status structures are used by the message receiving functions to return data about a message.
						 *  The structure contains the following fields:
						 *  - MPI_SOURCE - id of processor sending the message
						 *  - MPI_TAG - the message tag
						 *  - MPI_ERROR - error status */

	MPI_Datatype MPI_BVOXEL;
	MPI_Type_contiguous(2, MPI_UNSIGNED_SHORT, &MPI_BVOXEL); /* Creates a contiguous datatype */
	MPI_Type_commit(&MPI_BVOXEL); /* Commits the datatype */

	int send = 1;
	while(send > 0) {
		std::unordered_map<border_voxel, pair<Pocket *, size_t>> left_pockets, right_pockets;
		vector<border_voxel> toRight, toLeft, fromRight, fromLeft;
		unsigned long toRight_N = 0, toLeft_N = 0, fromRight_N = 0, fromLeft_N = 0;

		for (auto & p : pockets) {
			if (p.isPocket)
				continue;
			if (p.hasRightBorder()) {
				size_t N = p.rightBorder.size();
				for (size_t i = 0; i < N; ++i) {
					p.SA_right[i] = true;
					toRight.push_back(p.rightBorder[i].front());
					++toRight_N;
					for (auto const & rbv : p.rightBorder[i]) {
						right_pockets[rbv] = make_pair(&p, i);
					}
				}
			}
			if (p.hasLeftBorder()) {
				size_t N = p.leftBorder.size();
				for (size_t i = 0; i < N; ++i) {
					p.SA_left[i] = true;
					toLeft.push_back(p.leftBorder[i].front());
					++toLeft_N;
					for (auto const & lbv : p.leftBorder[i]) {
						left_pockets[lbv] = make_pair(&p, i);
					}
				}
			}
		}
		/**
		 * Synchronize
		 */
//		MPI_Barrier(MPI_COMM_WORLD);
		/**
		 * Communicate number of candidate pockets to the right
		 */
		if (rank < procs - 1)
			MPI_Send(&toRight_N, 1, MPI_UNSIGNED_LONG, rank + 1, 0, MPI_COMM_WORLD);
		if (rank > 0)
			MPI_Recv(&fromLeft_N, 1, MPI_UNSIGNED_LONG, rank - 1, 0, MPI_COMM_WORLD, &status);
		/**
		 * Communicate number of candidate pockets to the left
		 */
		if (rank > 0)
			MPI_Send(&toLeft_N, 1, MPI_UNSIGNED_LONG, rank - 1, 0, MPI_COMM_WORLD);
		if (rank < procs - 1)
			MPI_Recv(&fromRight_N, 1, MPI_UNSIGNED_LONG, rank + 1, 0, MPI_COMM_WORLD, &status);
		/**
		 * Synchronize
		 */
//		MPI_Barrier(MPI_COMM_WORLD);
		/**
		 * Communicate actual candidate pockets to the right
		 */
		if (rank < procs - 1 && toRight_N > 0)
			MPI_Send(&toRight[0], toRight_N, MPI_BVOXEL, rank + 1, 0, MPI_COMM_WORLD);
		if (rank > 0 && fromLeft_N > 0) {
			fromLeft.resize(fromLeft_N);
			MPI_Recv(&fromLeft[0], fromLeft_N, MPI_BVOXEL, rank - 1, 0, MPI_COMM_WORLD, &status);
		}
		/**
		 * Communicate actual candidate pockets to the left
		 */
		if (rank > 0 && toLeft_N > 0)
			MPI_Send(&toLeft[0], toLeft_N, MPI_BVOXEL, rank - 1, 0, MPI_COMM_WORLD);
		if (rank < procs - 1 && fromRight_N > 0) {
			fromRight.resize(fromRight_N);
			MPI_Recv(&fromRight[0], fromRight_N, MPI_BVOXEL, rank + 1, 0, MPI_COMM_WORLD, &status);
		}
		/**
		 * Synchronize
		 */
//		MPI_Barrier(MPI_COMM_WORLD);
		/**
		 * This value remains 0 if no pocket is resolved in the current slice.
		 * This MPI implementation has no Boolean variables, so we are using
		 * an integer.
		 */
		int resolved = 0; // false
		/**
		 * Resolve undetermined pockets with the information received from left
		 */
		for (auto const & bv : fromLeft) {
			auto it = left_pockets.find(bv);
			if (it != left_pockets.end()) {
				Pocket * p = it->second.first;
				size_t i = it->second.second;
				p->SA_left[i] = false;
			}
		}
		/**
		 * Resolve undetermined pockets with the information received from right
		 */
		for (auto const & bv : fromRight) {
			auto it = right_pockets.find(bv);
			if (it != right_pockets.end()) {
				Pocket * p = it->second.first;
				size_t i = it->second.second;
				p->SA_right[i] = false;
			}
		}
		for (auto & p : pockets) {
			if (p.isPocket) {
				continue;
			}
			bool SA_left = (find(p.SA_left.begin(), p.SA_left.end(), true) != p.SA_left.end()); //at least one value is true
			bool SA_right = (find(p.SA_right.begin(), p.SA_right.end(), true) != p.SA_right.end()); //at least one value is true
//			bool SA_left = accumulate(p.SA_left.begin(), p.SA_left.end(), false, [](bool const & a, bool const & b) { return a || b; });
//			bool SA_right = accumulate(p.SA_right.begin(), p.SA_right.end(), false, [](bool const & a, bool const & b) { return a || b; });
			if (SA_left || SA_right) {
				p.isPocket = true;
				resolved = 1; // true
			}
		}
		/**
		 * Synchronize
		 */
//		MPI_Barrier(MPI_COMM_WORLD);
		/**
		 * If at least one slice did identify one or more candidates as real
		 * pockets, repeat one more iteration of the algorithm.
		 * The algorithm stops if no new pockets are identified.
		 */
		MPI_Allreduce(&resolved, &send, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	}
	MPI_Type_free(&MPI_BVOXEL);
}


void MolecularSurface::outputSurfaceOpenDXModel(std::string const & filename) {
	std::ofstream file_stream;
	makeDirectory("./output");
	file_stream.open("./output/" + filename + ".dx");
	if (!file_stream.is_open()) {
		throw std::ofstream::failure("Error opening output file.");
	}
	size_t gridSize = plength * pwidth * pheight;

	file_stream  << std::setprecision(3);
	/* File header */
	file_stream << "# created with " << PROGRAM_NAME <<"\n";
	file_stream << "# version " << PROGRAM_VERSION <<"\n";
	file_stream << "object 1 class gridpositions counts " << plength << " "	<< pwidth << " " << pheight << "\n";
	file_stream << "origin " << -ptran.x << " " << -ptran.y << " " << -ptran.z << "\n";
	file_stream << "delta " << 1/resolution << " " << 0.0f << " " << 0.0f << "\n";
	file_stream << "delta " << 0.0f << " " << 1/resolution << " " << 0.0f << "\n";
	file_stream << "delta " << 0.0f << " " << 0.0f << " " << 1/resolution << "\n";
	file_stream << "object 2 class gridpositions counts " << plength << " "	<< pwidth << " " << pheight << "\n";
	file_stream << "object 3 class array type double rank 0 items "<< gridSize << " data follows\n";
	/* data */
	size_t c = 1;
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0; k < pheight; ++k) {
				if (surface->getVoxel(i, j, k))
					file_stream << 1.0f;
				else
					file_stream << 0.0f;
				if (c % 3 == 0 || c == gridSize) {
					file_stream << "\n";
				}
				else {
					file_stream << " ";
				}
				++c;
			}
		}
	}
	file_stream << "attribute \"dep\" string \"positions\"\n";
	file_stream	<< "object \"regular positions regular connections\" class field\n";
	file_stream << "component \"positions\" value 1\n";
	file_stream << "component \"connections\" value 2\n";
	file_stream << "component \"data\" value 3\n";
	file_stream.close();
} /* outputSurfacePCDModel() */

