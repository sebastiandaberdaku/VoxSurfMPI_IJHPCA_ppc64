/*
 * Molecule.cpp
 *
 *  Created on: 07/set/2014
 *      Author: sebastian
 */

#include "../utils/doubleCompare.h"
#include "../utils/makeDirectory.h"
#include "Molecule.h"
#include <chrono>

#include <mpi.h>

float Molecule::max_atm_radius = min_float;
float Molecule::min_atm_radius = max_float;

using namespace std;

Molecule::Molecule(int surfaceType, float probeRadius, float resolution, string const & inname, string const & outname) :
	centroid(point3D(0, 0, 0)) {
	int rank;
	int procs;
	MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if (!rank)
		cout << "Loading PDB file: "<< inname <<"\n";
	pdbModel = new PDBModel(inname);

	poseNormalization(pdbModel->atomsInModel, atoms);

	sort(atoms.begin(), atoms.end(),
			[](atom const & a, atom const & b){ return a.x < b.x; });//(a.x - a.radius) < (b.x - b.radius); });
	MPI_Barrier(MPI_COMM_WORLD);
    double t_start = MPI_Wtime();
	if (!rank) {
		cout << "Initializing parameters for " << inname << ".\n";
		cout << "Number of atoms in model: " << atoms.size() << "\n";
	}
	if (surfaceType == 1)
		MolecularSurface::boundingBox(atoms, false, probeRadius, resolution,
			length, width, height, translation, max_atm_radius, min_atm_radius);
	else
		MolecularSurface::boundingBox(atoms, true, probeRadius, resolution,
			length, width, height, translation, max_atm_radius, min_atm_radius);
    /**
     * We divide the workload in order to get roughly the same
     * atom volume in each slice.
     */
	vector<double> volumeRepartition;
	float totVolume = 0.0;
	for (auto const & atm : atoms) {
		totVolume += pow(atm.radius, 3.0);
		volumeRepartition.push_back(totVolume);
	}

	float perSliceVolume = totVolume / procs;

	auto lo = upper_bound(volumeRepartition.begin(), volumeRepartition.end(), rank * perSliceVolume);
	auto hi = upper_bound(volumeRepartition.begin(), volumeRepartition.end(), (rank + 1) * perSliceVolume);

	int idx_lo = lo - volumeRepartition.begin();
	int idx_hi = hi - volumeRepartition.begin();

	int lo_x = 0, hi_x = length - 1;

	if (surfaceType == 1) {
		if (rank > 0)
			lo_x = static_cast<int>(round((atoms[idx_lo].x + translation.x) * resolution));
//					- static_cast<int>(round(atoms[idx_lo].radius * resolution));
		if (rank < procs - 1)
			hi_x = static_cast<int>(round((atoms[idx_hi].x + translation.x) * resolution));
//					- static_cast<int>(round(atoms[idx_hi].radius * resolution));
	} else {
		if (rank > 0)
			lo_x = static_cast<int>(round((atoms[idx_lo].x + translation.x) * resolution));
//					- static_cast<int>(round((atoms[idx_lo].radius + probeRadius) * resolution));
		if (rank < procs - 1)
			hi_x = static_cast<int>(round((atoms[idx_hi].x + translation.x) * resolution));
//					- static_cast<int>(round((atoms[idx_hi].radius + probeRadius) * resolution));
	}

//	int dL = length / procs;
//
//	if (rank > 0)
//		lo_x = rank * dL;
//	if (rank < procs - 1)
//		hi_x = (rank + 1) * dL;

//	float perSliceVolume = totVolume / (procs - 1.5);
//
//	auto lo = upper_bound(volumeRepartition.begin(), volumeRepartition.end(), (rank - 0.75) * perSliceVolume);
//	auto hi = upper_bound(volumeRepartition.begin(), volumeRepartition.end(), (rank + 0.25) * perSliceVolume);
//
//	int idx_lo = lo - volumeRepartition.begin();
//	int idx_hi = hi - volumeRepartition.begin();
//
//	int lo_x = 0, hi_x = length - 1;
//
//	if (surfaceType == 1) {
//		if (rank > 0)
//			lo_x = static_cast<int>(round((atoms[idx_lo].x + translation.x) * resolution));
////					- static_cast<int>(round(atoms[idx_lo].radius * resolution));
//		if (rank < procs - 1)
//			hi_x = static_cast<int>(round((atoms[idx_hi].x + translation.x) * resolution));
////					- static_cast<int>(round(atoms[idx_hi].radius * resolution));
//	} else {
//		if (rank > 0)
//			lo_x = static_cast<int>(round((atoms[idx_lo].x + translation.x) * resolution));
////					- static_cast<int>(round((atoms[idx_lo].radius + probeRadius) * resolution));
//		if (rank < procs - 1)
//			hi_x = static_cast<int>(round((atoms[idx_hi].x + translation.x) * resolution));
////					- static_cast<int>(round((atoms[idx_hi].radius + probeRadius) * resolution));
//	}

	uint16_t sliceLength = hi_x - lo_x + 1;
	cout << "rank: " << rank << " : length: " << sliceLength <<endl;
	if (rank < procs - 1)
		cout << "rank: " << rank << " : %volume: " << (volumeRepartition[idx_hi] - volumeRepartition[idx_lo]) * 100.0 / totVolume  <<endl;
	else
		cout << "rank: " << rank << " : %volume: " << (totVolume - volumeRepartition[idx_lo]) * 100.0 / totVolume  <<endl;

	translation.x -= (lo_x / resolution);

	surface = new MolecularSurface(atoms, probeRadius, resolution, sliceLength, width, height, translation);

	/**
	 * Van der Waals surface calculation
	 */
	if (surfaceType == 1) {
		if (!rank)
			cout << "Calculating CPK Model." << endl;
		surface->createCPKModel(false);
		if (!rank)
			cout << "Building van der Waals surface." << endl;
		surface->buildSurface(false);
	} else if (surfaceType == 2) {
		if (!rank)
			cout << "Calculating CPK Model." << endl;
		surface->createCPKModel(true);
		if (!rank)
			cout << "Building Solvent Accessible surface." << endl;
		surface->buildSurface(true);
	} else {
		if (!rank)
			cout << "Calculating CPK Model." << endl;
	    surface->createCPKModel(true);
		if (!rank)
			cout << "Building Solvent Accessible surface." << endl;
		surface->buildSurface(true);;
		if (!rank)
			cout << "Calculating the Euclidean Distance Transform." << endl;
		surface->fastRegionGrowingEDT();
		if (procs > 1) {
			if (!rank)
				cout << "Extracting potential pocket voxels." << endl;
			surface->extractPotentialPockets();
		}
		if (!rank)
			cout << "Filling buried cavities and building Solvent Excluded surface." << endl;
		surface->removeSolventExcludedVoids();
		if (procs > 1) {
			if (!rank)
				cout << "Resolving undetermined pockets." << endl;
			surface->solveUndeterminedPockets();
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
	if (!rank)
		cout << "Surface calculation time:\t" << MPI_Wtime() - t_start << "\n";

//	surface->displaySurface();
#ifndef NO_OUTPUT_TEST
	/* Output PCD model to file */
	if (!rank)
		cout << "Output surface PCD model.\n";
//	surface->outputSurfaceOpenDXModel(outname+ "-" + to_string(rank));
	surface->outputSurfacePCDModel(outname + "-" + to_string(rank), RT, centroid);
	surface->outputSurfacePCDModel(outname + "-" + to_string(rank));
	if (procs > 1) {
		if (!rank)
			cout << "Output pockets PCD model.\n";
		surface->outputPocketsPCDModel(outname + "-" + to_string(rank), RT, centroid);
//		surface->outputPocketsPCDModel(outname + "-" + to_string(rank));
	}
#endif
}

Molecule::~Molecule() {
	delete surface;
	delete pdbModel;
}
