/*
 * DescriptorEvaluation.cpp
 *
 *  Created on: 25/sep/2015
 *      Author: Sebastian Daberdaku
 */

//#include "CommandLineParser/CommandLineParser.h"
#include "Molecule/Molecule.h"
#include "utils/disclaimer.h"
#include "utils/makeDirectory.h"
#include <iostream>
#include <numeric>
#include <stdlib.h>
#include <string>
#include <unordered_map>
#include <utility>
#include "exceptions/ParsingPDBException.h"

#include <mpi.h>

using namespace std;

int main (int argc, char* argv[]) {
//---------------------------variables and parameters------------------------------------
	float probeRadius; // probe sphere radius
	float resolution; // resolution^3 = #voxels/Å^3
	string inname_molecule; // input filename
	string outname_molecule; // output filename
	int surfaceType; // Surface type, 1-vdW 2-SAS 3-SES
	bool help = false; // if true, print the help message
	bool version = false; // if true, print the program version
	bool surf_description = false; // if true, print the three surface descriptions
	bool license = false; // if true, print the license information

	MPI_Status status; /**< MPI_Status structures are used by the message receiving functions to return data about a message.
						 *  The structure contains the following fields:
						 *  - MPI_SOURCE - id of processor sending the message
						 *  - MPI_TAG - the message tag
						 *  - MPI_ERROR - error status */
	int rank; /**< rank of the current process */
	int procs; /**< total number of processes */

	/* Initialize the MPI execution environment */
	MPI_Init(&argc, &argv);
	/* Determines the size of the group associated with a communicator */
	MPI_Comm_size(MPI_COMM_WORLD, &procs);
	/* Determines the rank of the calling process in the communicator */
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Barrier(MPI_COMM_WORLD);
    double const t_start = MPI_Wtime();

//	if (!CommandLineParser::parseCommandLine(argc, argv,
//			surfaceType, probeRadius, resolution,
//			inname_molecule, outname_molecule,
//			help, version, surf_description, license)) {
//		MPI_Finalize();
//		return EXIT_FAILURE;
//	}

    inname_molecule=argv[1];

    int lastindex = inname_molecule.find_last_of(".");
    outname_molecule = inname_molecule.substr(0, lastindex);
    surfaceType = stoi(argv[2]);
    probeRadius=stof(argv[3]);
    resolution=stof(argv[4]);
	if (help || version || surf_description || license) {
		MPI_Finalize();
		return EXIT_SUCCESS;
	}
//-----------------------------print config-------------------------------------------
	if (!rank) {
		PROGRAM_INFO
		/* summary of the parsed parameters */
		cout << "The specification is: \n";
		cout << "input filename: " << inname_molecule << "\n";
		cout << "output filename: " << outname_molecule << "\n";
		cout << "Surface type: ";
		switch (surfaceType) {
		case 1:  cout << "van der Waals\n"; break;
		case 2:  cout << "Solvent Accessible\n"; break;
		default: cout << "Solvent Excluded\n";
		}
		cout << "probe radius:\t" << probeRadius << "Å, \n";
		cout << "resolution:\t" << pow((double) resolution, 3.0) << " voxels per Å³, \n";
		cout << "Total number of processes: " << procs << "\n";
		cout << "**************************************************\n";
	}
//-----------------------------computation--------------------------------------------
	try {
		Molecule mol(surfaceType, probeRadius, resolution, inname_molecule, outname_molecule);
//-----------------------------conclusion --------------------------------------------
        MPI_Barrier(MPI_COMM_WORLD); /* stop all processes */
		if (!rank) {
			cout << "**************************************************\n";
			cout << "Total calculation time:\t" << MPI_Wtime() - t_start << "\n";
			cout << "**************************************************\n";
		}


	} catch (ParsingPDBException const & e) {
		if (!rank)
			cerr << "error: " << e.what() << "\n";
		MPI_Finalize();
		return EXIT_FAILURE;
	} catch (fstream::failure const & e) {
		if (!rank)
			cerr << "error: " << e.what() << "\n";
		MPI_Finalize();
		return EXIT_FAILURE;
	} catch (out_of_range const & e) {
		if (!rank)
			cerr << "error: " << e.what() << "\n";
		MPI_Finalize();
		return EXIT_FAILURE;
	} catch (invalid_argument const & e) {
		if (!rank)
			cerr << "error: " << e.what() << "\n";
		MPI_Finalize();
		return EXIT_FAILURE;
	} catch (logic_error const & e) {
		if (!rank)
			cerr << "error: " << e.what() << "\n";
		MPI_Finalize();
		return EXIT_FAILURE;
	} catch (runtime_error const & e) {
		if (!rank)
			cerr << "error: " << e.what() << "\n";
		MPI_Finalize();
		return EXIT_FAILURE;
	}
	MPI_Finalize();
	return EXIT_SUCCESS;
}
