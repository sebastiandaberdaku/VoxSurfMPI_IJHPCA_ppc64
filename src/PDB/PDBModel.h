/**
 * In this header we define the PDBModel class and relative methods.
 */

#ifndef PDBMODEL_H_
#define PDBMODEL_H_

#include "../Geometry/point3D.h"
#include <vector>
#include "../PDB/atom.h"

typedef enum lineType {
	ATOM, HETATM, OTHER_LN, TER, REMARK, END
} lineType;

class PDBModel {
public:
	size_t line_num;
	std::vector<atom> atomsInModel;
	std::string filename;
	std::string header;

	PDBModel(string const & filename);
	PDBModel(PDBModel const & model);
	/**
	 * Copy assignment operator
	 */
	PDBModel & operator=(PDBModel const & model);

private:
	lineType getLineType(string const & line);
	void loadFile(string const & filename);
	void parseLine(string const & line);
};
#endif /* PDBMODEL_H_ */
