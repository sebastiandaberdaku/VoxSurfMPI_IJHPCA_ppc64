/*
 * border_offset.h
 *
 *  Created on: Nov 2, 2015
 *      Author: sebastian
 */

#ifndef BORDER_OFFSET_H_
#define BORDER_OFFSET_H_
#include <inttypes.h>
#include <vector>
using namespace std;

struct border_voxel;

typedef struct border_offset {
	border_offset();
	border_offset(border_offset const & o);
	border_offset(border_voxel const & o);
	border_offset(int32_t iy, int32_t iz);
	border_offset& operator=(border_offset const & s);

	int32_t iy, iz;
	inline border_offset & operator+=(border_offset const & rhs) {
		this->iy += rhs.iy;
		this->iz += rhs.iz;
		return *this;
	};
	inline const border_offset operator+(border_offset const & rhs) const {
		return border_offset(*this) += rhs;
	};
	inline const border_offset operator-() const {
		return border_offset(-(this->iy), -(this->iz));
	};
	inline border_offset & operator-=(border_offset const & rhs) {
		this->iy -= rhs.iy;
		this->iz -= rhs.iz;
		return *this;
	};
	inline const border_offset operator-(border_offset const & rhs) const {
		return border_offset(*this) -= rhs;
	};
	static const vector<border_offset> neighbours;
}border_offset;


#endif /* BORDER_OFFSET_H_ */
