/*
 * border_offset.cpp
 *
 *  Created on: Nov 2, 2015
 *      Author: sebastian
 */

#include "border_offset.h"
#include "border_voxel.h"

border_offset::border_offset() : iy(0), iz(0) { };
border_offset::border_offset(border_offset const & o) : iy(o.iy), iz(o.iz) { };
border_offset::border_offset(border_voxel const & o) : iy(o.iy), iz(o.iz) { };

border_offset::border_offset(int32_t iy, int32_t iz): iy(iy), iz(iz) { };
border_offset& border_offset::operator=(border_offset const & s) {
	if (this != &s) {
		this->iy = s.iy;
		this->iz = s.iz;
	}
	return *this;
};

const vector<border_offset> border_offset::neighbours = {
		/** Neighbours sharing one face with the current voxel. */
		border_offset( -1,   1), border_offset(  0,   1), border_offset(  1,   1),
		border_offset( -1,   0), 						  border_offset(  1,   0),
		border_offset( -1,  -1), border_offset(  0,  -1), border_offset(  1,  -1),
		border_offset(  0,   0)
};
