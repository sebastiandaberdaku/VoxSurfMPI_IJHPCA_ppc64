/*
 * border_voxel.cpp
 *
 *  Created on: Sep 25, 2015
 *      Author: sebastian
 */

#include "border_voxel.h"

border_voxel::border_voxel() :
		iy(0), iz(0) { }

border_voxel::border_voxel(voxel const & v) :
		iy(v.iy), iz(v.iz) { }

border_voxel::border_voxel(uint16_t iy, uint16_t iz) :
		iy(iy), iz(iz) { }

bool border_voxel::operator==(border_voxel const & rhs) const {
	return (this->iy == rhs.iy) && (this->iz == rhs.iz);
}

border_voxel & border_voxel::operator=(border_voxel const & vox) {
	if (this != &vox) {
		this->iy = vox.iy;
		this->iz = vox.iz;
	}
	return *this;
}

