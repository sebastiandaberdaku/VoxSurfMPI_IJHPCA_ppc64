/*
 * border_voxel.h
 *
 *  Created on: Sep 25, 2015
 *      Author: sebastian
 */

#ifndef BORDER_VOXEL_H_
#define BORDER_VOXEL_H_

#include <boost/functional/hash.hpp>
#include "voxel.h"

#include "border_offset.h"

using namespace std;

typedef struct border_voxel{
	border_voxel();
	border_voxel(voxel const & v);
	border_voxel(uint16_t iy, uint16_t iz);
	bool operator==(border_voxel const & rhs) const;
	border_voxel & operator=(border_voxel const & vox);
	uint16_t iy, iz;

	inline border_voxel & operator+=(border_offset const & rhs) {
		this->iy += rhs.iy;
		this->iz += rhs.iz;
		return *this;
	};
	inline const border_voxel operator+(border_offset const & rhs) const {
		return border_voxel(*this) += rhs;
	};
	inline border_voxel & operator-=(border_offset const & rhs) {
		this->iy -= rhs.iy;
		this->iz -= rhs.iz;
		return *this;
	};
	inline const border_voxel operator-(border_offset const & rhs) const {
		return border_voxel(*this) -= rhs;
	};
} border_voxel;

namespace std {

template<>
struct hash<border_voxel> {
	size_t operator()(border_voxel const & p) const {
		size_t result = hash<uint16_t>()(p.iy);
		boost::hash_combine(result, p.iz);
		return result;
	}
};

template<>
struct equal_to<border_voxel> {
	bool operator()(border_voxel const & p, border_voxel const & q) const {
		return p == q;
	}
};
}


#endif /* BORDER_VOXEL_H_ */
