/*
 * dmap_value.h
 *
 *  Created on: Sep 26, 2015
 *      Author: sebastian
 */

#ifndef DMAP_VALUE_H_
#define DMAP_VALUE_H_

typedef struct dmap_value {
	dmap_value();
	dmap_value(dmap_value const & dmv);
	dmap_value(int ix, int iy, int iz, int d, int nx, int ny, int nz);
	dmap_value & operator=(dmap_value const & dmv);
	int ix, iy, iz; // current voxel propagating the distance map value
	int d; // current distance map value
	int nx, ny, nz;
} dmap_value;



#endif /* DMAP_VALUE_H_ */
